const amqp = require('amqplib/callback_api');
var fs = require('fs');

amqp.connect("amqps://equipo1:12characters@b-7cb6cbe0-23c5-40f6-85a8-29e06b4d8b48.mq.us-east-1.amazonaws.com:5671", (err, con) => {
	if(err) {
		throw err;
	}

	con.createChannel((err1, channel) => {
		if(err1) {
			throw err1;
		}

		let queue = "instance_a";

		channel.assertQueue(queue, {
			durable: false
		});

		channel.consume(queue, (message) => {
			let msg = message.content.toString();
			fs.appendFile("./queue/dump.txt", (msg+"\n"), () => {});
		}, { noAck: true });

	});

});