# mq-project-cc
Implementación de una arquitectura de alta disponibilidad basada en mensajería.

## Integrantes

> 329610  Luis Fernando Félix Mata

> 329748  Julián Terán Vázquez
### Descripción
Nuestra aplicación comienza desde las aplicaciones web alojadas en cada una de las 
instancias de EC2. Desde la aplicación de envío de mensajes, los mensajes 
comunicados a nuestro broker de Amazon MQ mediante una petición HTTP que 
recibe nuestra función Lambda. Como parámetros de la función se encuentran tanto 
la cola que será utilizada como el cuerpo del mensaje. Finalmente, la aplicación web 
de recepción de mensajes lee la cola del broker de RabbitMQ y almacena los 
mensajes en el directorio de EFS para su posterior lectura.

### Diagrama de la aplicación
![diagram](https://i.imgur.com/W3tTyxF.png)

### Ejemplo de archivo JSON
La función de AWS Lambda (```https://geutcdrrbb.execute-api.us-east-1.amazonaws.com/send_message```) recibe un archivo JSON como el que se incluye a continuación como cuerpo de la petición HTTP:
```javascript
{
    "queue":"instance_a",
    "message":"123"
}
```
